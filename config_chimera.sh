#!/bin/bash
export ARCH=arm
if [ ! -f /data/iCoreM6/imx-android-13.4.1/myandroid/prebuilt/linux-x86/toolchain/arm-eabi-4.4.3/bin/arm-eabi- ]
	export CROSS_COMPILE=/data/iCoreM6/imx-android-13.4.1/myandroid/prebuilt/linux-x86/toolchain/arm-eabi-4.4.3/bin/arm-eabi-
then
	export CROSS_COMPILE=/opt/freescale/usr/local/gcc-4.6.2-glibc-2.13-linaro-multilib-2011.12/fsl-linaro-toolchain/bin/arm-none-linux-gnueabi-
	export PATH=$PATH:/opt/freescale/usr/local/gcc-4.6.2-glibc-2.13-linaro-multilib-2011.12/fsl-linaro-toolchain/bin
fi
make distclean
make vybrid_nand_config	
echo `getconf _NPROCESSORS_ONLN`
make -j `getconf _NPROCESSORS_ONLN` u-boot.imx

dd if=/dev/zero of=u-boot-pad bs=1024 count=1
cat u-boot-pad u-boot.imx > u-boot.nand
cp u-boot.nand /tftp_boot

